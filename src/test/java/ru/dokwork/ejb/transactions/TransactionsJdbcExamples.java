package ru.dokwork.ejb.transactions;

import org.h2.jdbc.JdbcSQLException;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionsJdbcExamples {

    private static JdbcDataSource ds;
    private MessageDao messagesDao;

    @BeforeClass
    public static void createDataSource() throws SQLException {
        ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        ds.setUser("sa");
        ds.setPassword("sa");
    }

    @Before
    public void createDao() throws Exception {
        messagesDao = new MessageDao(ds);
        messagesDao.deleteAll();
    }

    @Test
    public void testAutoCommitted() throws Exception {
        String sql = "INSERT INTO messages (text) VALUES ('Hello')";
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            ps.execute();
        } // здесь подключение будет закрыто и транзакция подтверждена
        Assert.assertTrue(messagesDao.isContains("Hello"));
    }

    @Test
    public void testCustomCommitted() throws Exception {
        String sql = "INSERT INTO messages (text) VALUES ('Hello')";
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            con.setAutoCommit(false);
            ps.execute();
            // подтверждаем транзакцию
            con.commit();
        } // здесь подключение будет закрыто, и т.к. транзакция
        // была подтверждена, сообщение в базу попадет
        Assert.assertTrue(messagesDao.isContains("Hello"));
    }

    @Test(expected = JdbcSQLException.class)
    public void testCustomNotCommitted() throws Exception {
        String sql = "INSERT INTO messages (text) VALUES ('Hello')";
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            con.setAutoCommit(false);
            ps.execute();
        } // здесь подключение будет закрыто, но транзакция останется не
        // закрытой, поэтому сообщение в базу не попадет
        Assert.assertFalse(messagesDao.isContains("Hello"));
        // Более того, подключение будет закрыто, а транзакция останется
        // открытой и продолжит удерживать блокировку на строке 'Hello'!
        // Поэтому повторная попытка вставить сообщение вызовет исключение:
        messagesDao.insert("Hello"); // throws JdbcSQLException
    }
}
