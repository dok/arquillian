package ru.dokwork.ejb.transactions;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.sql.DataSource;

@RunWith(Arquillian.class)
public class BeanTransactionManagementTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClasses(BeanTransactionManagementEjb.class, MessageDao
                        .class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

    @Resource(lookup = "java:/xa-test")
    DataSource ds;

    @EJB
    BeanTransactionManagementEjb ejb;

    MessageDao messagesDao;

    @Before
    public void prepareDataBase() throws Exception {
        messagesDao = new MessageDao(ds);
        messagesDao.deleteAll();
    }

    @Test
    public void testCommitInsert() throws Exception {
        // when:
        //    tx.begin();
        //    messagesDao.insert("Hello");
        //    tx.commit();
        ejb.commitInsert("Hello");
        // then:
        Assert.assertTrue(messagesDao.isContains("Hello"));
    }

    @Test
    public void testRollbackInsert() throws Exception {
        // when:
        //    tx.begin();
        //    messagesDao.insert("Hello");
        //    tx.rollback();
        ejb.rollbackInsert("Hello");
        // then:
        Assert.assertFalse(messagesDao.isContains("Hello"));
    }
}