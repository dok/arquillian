package ru.dokwork.ejb.transactions;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.sql.DataSource;

@RunWith(Arquillian.class)
public class TransactionsExamples {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClasses(BeanTransactionManagementEjb.class, ContainerTransactionManagementEjb.class, MessageDao.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

    @Resource(lookup = "java:/xa-test")
    DataSource ds;

    @EJB
    ContainerTransactionManagementEjb containerManagement;

    @EJB
    BeanTransactionManagementEjb userManagement;

    MessageDao messagesDao;

    @Before
    public void prepareDataBase() throws Exception {
        messagesDao = new MessageDao(ds);
        messagesDao.deleteAll();
    }

    @Test
    public void testWithoutException() throws Exception {
        // when:
        containerManagement.insertWithoutException("Hello");
        // then:
        Assert.assertTrue(messagesDao.isContains("Hello"));
    }

    /**
     * Из-за RuntimeException транзакция будет откатана.
     */
    @Test
    public void testWithRuntimeException() throws Exception {
        Throwable error = null;
        // when:
        try {
            containerManagement.insertWithRuntimeException("Hello");
        } catch (Throwable e) {
            error = e;
        }
        // then:
        Assert.assertNotNull(error);
        Assert.assertEquals(error.getClass(), EJBException.class);
        Assert.assertFalse(messagesDao.isContains("Hello"));
    }

    /**
     * Несмотря на Exception транзакция будет завершена
     */
    @Test
    public void testWithCheckedException() throws Exception {
        Throwable error = null;
        // when:
        try {
            containerManagement.insertWithCheckedException("Hello");
        } catch (Throwable e) {
            error = e;
        }
        // then:
        Assert.assertNotNull(error);
        Assert.assertTrue(messagesDao.isContains("Hello"));
    }

    @Test
    public void shouldFail() throws Exception {
        Assert.fail();
    }
}
