package ru.dokwork.ejb.transactions;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageDao {

    private final DataSource ds;

    public boolean isContains(String message) throws SQLException {
        final String sql =
                "SELECT count(*) FROM messages WHERE text = ?";
        int count = 0;
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, message);
            final ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        }
        return count > 0;
    }

    public void insert(String message) throws SQLException {
        final String sql = "INSERT INTO messages (text) VALUES(?)";
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, message);
            ps.executeUpdate();
        }
    }

    public void deleteAll() throws SQLException {
        final String sql = "DELETE FROM messages";
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            ps.executeUpdate();
        }
    }

    public MessageDao(DataSource ds) throws SQLException {
        this.ds = ds;
        if (!isTableExist()) {
            createTable();
        }
    }

    private boolean isTableExist() throws SQLException {
        final String sql = "SELECT count(*) " +
                "FROM INFORMATION_SCHEMA.TABLES " +
                "WHERE lower(table_name) = 'messages'";
        int count = 0;
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        }
        return count > 0;
    }

    private void createTable() throws SQLException {
        final String sql = "CREATE TABLE messages (" +
                "text varchar(255) PRIMARY KEY)";
        try (final Connection con = ds.getConnection();
             final PreparedStatement ps = con.prepareStatement(sql)) {
            ps.execute();
        }
    }
}
