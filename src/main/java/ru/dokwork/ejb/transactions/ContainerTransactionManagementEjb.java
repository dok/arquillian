package ru.dokwork.ejb.transactions;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.SQLException;

@Stateless
public class ContainerTransactionManagementEjb {

    @Resource(lookup = "java:/xa-test")
    private DataSource ds;

    private MessageDao dao;

    @PostConstruct
    private void createDao() throws SQLException {
        dao = new MessageDao(ds);
    }

    public void insertWithoutException(String message) throws Exception {
        dao.insert(message);
    }

    public void insertWithRuntimeException(String message) throws Exception {
        dao.insert(message);
        throw new RuntimeException("Some runtime exception");
    }

    public void insertWithCheckedException(String message) throws Exception {
        dao.insert(message);
        throw new Exception("Some checked exception");
    }
}
