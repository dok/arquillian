package ru.dokwork.ejb.transactions;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import java.sql.SQLException;

@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
public class BeanTransactionManagementEjb {

    @Resource(lookup = "java:/xa-test")
    private DataSource ds;

    private MessageDao dao;

    @Resource
    private UserTransaction tx;

    @PostConstruct
    private void createDao() throws SQLException {
        dao = new MessageDao(ds);
    }

    public void commitInsert(String message) throws Exception {
        tx.begin();
        dao.insert(message);
        tx.commit();
    }

    public void rollbackInsert(String message) throws Exception {
        tx.begin();
        dao.insert(message);
        tx.rollback();
    }
}
